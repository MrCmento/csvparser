#pragma once

#include "types.h"

#include "stdint.h"
#include <memory>
#include <map>
#include <set>

#include "util.h" // because of split.. things like this should be in util...

namespace sql
{
    struct Connection; // forward
}

namespace database
{

struct DatabaseConnector;

typedef std::shared_ptr<DatabaseConnector> DatabaseConnectorPtr;

struct DatabaseConnector
{

    virtual ~DatabaseConnector();
    DatabaseConnector() = default;
    DatabaseConnector( DatabaseConnector const &  other ) = delete;
    DatabaseConnector &operator=( DatabaseConnector const &  other ) = delete;
    DatabaseConnector &operator=( DatabaseConnector && other ) = delete;

    bool connect( std::string const & address,
                  std::string const & user,
                  std::string const & password );

    void createTable( std::string const & tableName, DbDescr dbDescr );
    void dropTable( std::string const & table );

    void insert( std::string const & tableName,
                 Row const & row,
                 bool upsertMode = false );

    void createDatabase( std::string const & dBName );
    void dropDatabase( std::string const & dbName );
    void useDatabase( std::string const & dbName );

private:
    bool checkForbidden(const std::string &keywords ... );
    std::set<std::string> forbidden = {"DROP","ALTER"}; // sanitation
    util::PatchTable replacement = { { "IDENTITY", "AUTO_INCREMENT" } }; // keywords not provided by db platform

    sql::Connection *con = nullptr; // is also state for connection
};

}
