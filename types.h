#ifndef TYPES_H
#define TYPES_H

#include <vector>
#include <string>

typedef std::vector<std::string> Row;
typedef std::vector<Row> Table;
typedef std::vector<std::pair<std::string,std::string>> DbDescr;

#endif // TYPES_H
