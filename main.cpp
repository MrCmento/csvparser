#include <iostream>
#include "csvparser.h"
#include "databaseconnector.h"
#include "util.h"
#include <vector>
#include <thread>

using namespace std;
using namespace database;

string pass = ""; // for MySql db
string user = "clemens"; // of MySql db
string databaseName = "CSV_PARSER_DB"; // to be deleted and created
string address = "tcp://127.0.0.1:3306"; // connection details for db driver

int main(int argc, char *argv[])
{
    if( argc < 2 )
    {
        cout << "CSV Path must be provided !" << endl;
        exit(1);
    }
    if( argc < 3 )
    {
        cout << "Table name must be provided !" << endl;
        exit(1);
    }

    int threads = 0;

    if( argc > 3 )
        threads = stoi( argv[3]);

    if( threads < 1 )
        threads = 1;

    string path(argv[1]);
    string tabName(argv[2]);

    auto result = csvparser::parseCsvFile( path );
    auto const & toc = result.first;
    auto const & table = result.second;

    vector<vector<Row>> jobs = util::splitVector(table, threads );

    int schedule = std::min( threads, (int)jobs.size() );

    thread t[schedule];
    DatabaseConnector dbc[schedule];

    //Init der DB
    DatabaseConnector dbcCreator;
    dbcCreator.connect(address, user, pass);
    dbcCreator.dropDatabase( databaseName );
    dbcCreator.createDatabase( databaseName );
    dbcCreator.useDatabase( databaseName );
    dbcCreator.createTable( tabName , toc );

    for( int i = 0; i<schedule; ++i )
    {
        t[i] = thread([i,&jobs, &dbc, &tabName](){
            dbc[i].connect(address, user, pass);
            dbc[i].useDatabase( databaseName );

            for( auto const & tabEntry: jobs[i] )
                dbc[i].insert( tabName, tabEntry );
        });
    }

    for ( int i = 0; i < schedule; ++i )
        t[i].join();

    return 0;
}
