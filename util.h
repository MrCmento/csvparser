#pragma once

#include <string>
#include <vector>
#include <map>

#include "types.h"

namespace util
{

typedef std::map<std::string,std::string> PatchTable;

std::vector<std::string> split( std::string const & content, std::string const & delimiter );
std::vector<std::string> split( std::string const & content, char const & delimiter );
std::string replaceKeyWords( std::string const & in, PatchTable const & patch );

template<typename T>
std::vector<std::vector<T>> splitVector(const std::vector<T>& vec, size_t n);

//template must be visible in compilation unit...
extern template std::vector<std::vector<Row>> splitVector(const std::vector<Row> & vec, size_t n);

}

