#include "util.h"

#include <sstream>

using namespace std;

namespace util
{

vector<string> split( string const & content, string const & delimiter )
{
    vector<string> sep;
    size_t end = 0;
    size_t start = content.find_first_not_of( delimiter );

    while( (end = content.find_first_of( delimiter, start ) ) != string::npos )
    {
        sep.push_back( content.substr(start, end - start) );
        start = content.find_first_not_of( delimiter, end );
    }

    if( start != string::npos )
        sep.push_back(content.substr(start));

    return sep;
}

vector<string> split( string const & content, char const & delimiter )
{
    vector<string> sep;
    istringstream iss( content );
    string val;
    while( getline( iss, val, delimiter ) )
        sep.push_back( val );
    return sep;
}

string replaceKeyWords( string const & in, PatchTable const & patch )
{
    string result;
    auto parts = split( in, ' ' );
    size_t pos = 0;
    for( auto const & part : parts )
    {
        auto const & it = patch.find( part );

        if( it != patch.end() )
            result += it->second + ( ++pos != parts.size() ? " " : "" );
        else
            result += part + ( ++pos != parts.size() ? " " : "" );

    }
    return result;
}

template<typename T>
vector<vector<T>> splitVector(const std::vector<T>& vec, size_t n)
{
    std::vector<std::vector<T>> outVec;

    size_t length = vec.size() / n;
    size_t remain = vec.size() % n;

    size_t begin = 0;
    size_t end = 0;

    for (size_t i = 0; i < std::min(n, vec.size()); ++i)
    {
        end += (remain > 0) ? (length + !!(remain--)) : length;
        outVec.push_back(std::vector<T>(vec.begin() + begin, vec.begin() + end));
        begin = end;
    }

    return outVec;
}

template vector<vector<Row>> splitVector(const vector<Row> & vec, size_t n);

}

