#include "csvparser.h"

#include <fstream>
#include <algorithm>
#include <assert.h>
#include <sstream>

#include "util.h"

using namespace std;
using namespace csvparser;

namespace csvparser
{

pair<DbDescr,Table> parseCsvFile( string const & path )
{
    Table tab;
    ifstream in;
    string entry;

    const char delimiterCSV = ';';
    const string delimiterToc = "$$";

    try
    {
        in.open( path );
    }
    catch( exception const & )
    {
        throw std::runtime_error("cannot opend csv-file");
    }

    getline( in, entry ); // first line containing row description of csv

    DbDescr descr;
    auto const tocLine = util::split( entry, delimiterCSV );

    std::transform(tocLine.begin(), tocLine.end(), std::back_inserter(descr),
                      [=](const string & str){
        auto const namedType = util::split(str, delimiterToc );

        if( namedType.size() != 2 )
            throw std::runtime_error("csv-file contains wrong schema. Must be NAME$$Type");
        return make_pair( namedType[0], namedType[1] );
    });

    while ( getline( in, entry ) )
    {
        if( entry.empty() )
            continue;
        tab.push_back( util::split( entry, delimiterCSV ) );
    }

    return { descr, tab };
}

}
