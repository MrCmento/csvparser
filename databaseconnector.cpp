#include "databaseconnector.h"

#include "mysql_connection.h"
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

using namespace std;

#define CHECK \
    do{       \
        if( !con ) \
            throw std::runtime_error("No connection to Database established !"); \
    }while(false);


namespace
{

bool executeGeneric( sql::Connection *con, string const & statement )
{
    try
    {
        auto stmt = con->createStatement();
        stmt->execute( statement );
        delete stmt;
    }catch ( sql::SQLException &e ){
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return false;
    }
    return true;
}

}

namespace database
{

bool DatabaseConnector::connect( string const & address,
                                 string const & user,
                                 string const & password )
{
    try
    {
        if( con )
            throw std::runtime_error( "already connected");

        con = get_driver_instance()->connect( address, user, password); //IP Address, user name, password

    } catch ( sql::SQLException &e ){
        cout << "# ERR: " << e.what();
        cout << " (MySQL error code: " << e.getErrorCode();
        cout << ", SQLState: " << e.getSQLState() << " )" << endl;
        return false;
    }

    return con;
}

void DatabaseConnector::dropTable( string const & table )
{
    CHECK;
    string const statement = "DROP TABLE IF EXISTS "+table;
    executeGeneric( con, statement );
}

void DatabaseConnector::createDatabase( string const & dBName )
{
    CHECK;
    string const statement = "CREATE DATABASE IF NOT EXISTS "+ dBName;
    executeGeneric( con, statement );
}

void DatabaseConnector::dropDatabase( string const & dbName )
{
    CHECK;
    string const statement = "DROP DATABASE IF EXISTS "+ dbName;
    executeGeneric( con, statement );
}


void DatabaseConnector::useDatabase( string const & dbName )
{
    CHECK;
    string const statement = "USE "+ dbName;
    executeGeneric( con, statement );
}

DatabaseConnector::~DatabaseConnector()
{
    if( con )
        delete con;
}

bool DatabaseConnector::checkForbidden( string const & keywords ... )
{
    for( auto const & keyword : {keywords} )
        if( forbidden.count( keyword ) )
            return true;
    return false;
}

void DatabaseConnector::createTable( string const & tableName, DbDescr dbDescr )
{
    if( dbDescr.empty() )
        throw std::runtime_error("DbSchema insufficient!");

    string result;

    for( size_t i = 0; i<dbDescr.size(); ++i )
    {
        auto const & s = dbDescr[i];
        string const & name = s.first;
        string const & type = s.second;

        if( checkForbidden( name, type ) )
            throw exception();

        result += name + " " + util::replaceKeyWords( type, replacement ) + ",\n";
    }

    result += "CONSTRAINT PK_"+tableName+" PRIMARY KEY ("+ dbDescr.front().first +")";

    string command = "CREATE TABLE " + tableName + " (\n" + result + "\n);";

    CHECK;
    executeGeneric( con, command );

}

void DatabaseConnector::insert( std::string const & tableName,
             Row const & row,
             bool upsertMode )
{
    string result;

    int end = row.size() - 1;
    for( int i = 0; i<(int)row.size(); ++i )
    {
        auto const & s = row[i];
        if( checkForbidden(s) )
            throw exception();

        result += "'" + s + "'" + ( i != end ? ", " : "" );
    }

    string command = "INSERT INTO " + tableName + "\nVALUES (" + result  + ");";

    CHECK;
    executeGeneric( con, command );
}

}
