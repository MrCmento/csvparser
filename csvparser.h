#pragma once

#include "types.h"

namespace csvparser
{

std::pair<DbDescr,Table> parseCsvFile( std::string const & path );

}
